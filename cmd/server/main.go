package main

import (
	"log"
	"os"

	"github.com/urfave/cli"
	"gitlab.com/chespinoza/file-webservice/pkg/api"
)

func main() {

	log.SetFlags(log.LstdFlags | log.Lshortfile)

	app := cli.NewApp()
	app.Name = "file-webservice"
	app.Version = "0.0.1"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:   "addr",
			Value:  ":8080",
			Usage:  "Address to serve",
			EnvVar: "ADDR,SERVER_ADDR",
		},
		cli.StringFlag{
			Name:   "storage-path",
			Usage:  "Address to serve",
			EnvVar: "STORAGE_PATH",
		},
	}

	app.Action = func(ctx *cli.Context) error {
		webService := api.New(ctx.String("addr"), ctx.String("storage-path"))

		log.Println("Starting webservice")
		if err := webService.Start(); err != nil {
			return (err)
		}
		return nil
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}

}
