# File Web Service

[![Build Status](https://gitlab.com/chespinoza/file-webservice/badges/master/build.svg)](https://gitlab.com/chespinoza/file-webservice/commits/master)

## How to run basic tests
`make test`

## How to build
`make build`

## How to run 
`SERVER_ADDR=:8080 STORAGE_PATH=$PWD/filesystem build/file-service`

## Build docker image
`make docker`

## How to use:

### Create files:
`curl -i -H "Content-type:application/json" -X POST http://localhost:8080/api/v1/files -d '{"file_name":"file.txt", "path":"/dir1", "content":"aaa aaa"}'`

```
HTTP/1.1 200 OK
Content-Type: application/json
Date: Tue, 21 Aug 2018 13:52:12 GMT
Content-Length: 17

{"response":"ok"}%
```

`curl -i -H "Content-type:application/json" -X POST http://localhost:8080/api/v1/files -d '{"file_name":"file2.txt", "path":"/dir1", "content":"hi there"}'`

```
Content-Type: application/json
Date: Tue, 21 Aug 2018 13:54:14 GMT
Content-Length: 17

{"response":"ok"}%
```

### Request a file

`curl -i -H "Content-type:application/json" -X GET http://localhost:8080/api/v1/files -d '{"file_name":"file.txt", "path":"/dir1"}'`

```
HTTP/1.1 200 OK
Content-Type: application/json
Date: Tue, 21 Aug 2018 13:56:21 GMT
Content-Length: 9

"aaa aaa"%
```

### Replace content of a file

`curl -i -H "Content-type:application/json" -X PUT http://localhost:8080/api/v1/files -d '{"file_name":"file.txt", "path":"/dir1", "content":"new content"}'`

```
HTTP/1.1 200 OK
Content-Type: application/json
Date: Tue, 21 Aug 2018 14:00:11 GMT
Content-Length: 17

{"response":"ok"}%
```

### Delete a file
`curl -i -H "Content-type:application/json" -X DELETE http://localhost:8080/api/v1/files -d '{"file_name":"file.txt", "path":"/dir1"}'`

```
HTTP/1.1 200 OK
Content-Type: application/json
Date: Tue, 21 Aug 2018 14:01:45 GMT
Content-Length: 17

{"response":"ok"}%
```

### Get stats from folder
`curl -i -H "Content-type:application/json" -X GET http://localhost:8080/api/v1/files/stats -d '{"folder_name":"/dir1"}'`

```
HTTP/1.1 200 OK
Content-Type: application/json
Date: Tue, 21 Aug 2018 14:03:48 GMT
Content-Length: 144

{"number_of_files":1,"alphanum_avg":7,"alphanum_std_dev":0,"words_len_avg":3.5,"words_len_std_dev":2.1213203435596424,"Words":2,"total_bytes":8}%
```



