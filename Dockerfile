FROM golang:alpine AS builder
RUN apk --no-cache add \
    build-base \
    bash \
    git
ADD . /go/src/gitlab.com/chespinoza/file-webservice
WORKDIR /go/src/gitlab.com/chespinoza/file-webservice
RUN echo 'Building binary with: ' && go version
RUN make build

FROM alpine
RUN apk --no-cache add \
    ca-certificates
WORKDIR /app
EXPOSE 8080
COPY --from=builder /go/src/gitlab.com/chespinoza/file-webservice/build/file-service /app/
CMD /app/file-service start
