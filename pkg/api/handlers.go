package api

import (
	"encoding/json"
	"log"
	"net/http"
)

type jsonPayload struct {
	FileName string `json:"file_name"`
	Path     string `json:"path"`
}

// payload: {"file_name":"file.txt", "path":"/dir1", "content":"aaa aaa"}
func newFileHandler(w http.ResponseWriter, r *http.Request) {
	payload := struct {
		jsonPayload
		Content string `json:"content"`
	}{}

	if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
		log.Println("error decoding JSON payload:", err.Error())
		log.Println(r.Body)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	err := storageService.Create(payload.FileName, payload.Path, []byte(payload.Content))
	if err != nil {
		log.Println("error creating file:", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	writeJSON(w, struct {
		Result string `json:"response"`
	}{"ok"})
}

// payload: {"file_name":"file.txt", "path":"/dir1"}
func getFileHandler(w http.ResponseWriter, r *http.Request) {
	var payload jsonPayload
	if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
		log.Println("error decoding JSON payload:", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	content, err := storageService.Get(payload.FileName, payload.Path)
	if err != nil {
		log.Println("error retrieving file:", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	writeJSON(w, string(content))
}

// payload: {"file_name":"file.txt", "path":"/dir1", "content":"new content"}
func replaceFileHandler(w http.ResponseWriter, r *http.Request) {
	payload := struct {
		jsonPayload
		Content string `json:"content"`
	}{}

	if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
		log.Println("error decoding JSON payload:", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	err := storageService.Replace(payload.FileName, payload.Path, []byte(payload.Content))
	if err != nil {
		log.Println("error replacing file:", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	writeJSON(w, struct {
		Result string `json:"response"`
	}{"ok"})
}

// payload: {"file_name":"file.txt", "path":"/dir1"}
func deleteFileHandler(w http.ResponseWriter, r *http.Request) {
	var payload jsonPayload

	if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
		log.Println("error decoding JSON payload:", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	err := storageService.Delete(payload.FileName, payload.Path)
	if err != nil {
		log.Println("error replacing file:", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	writeJSON(w, struct {
		Result string `json:"response"`
	}{"ok"})
}

// payload: {"folder_name":"/dir1"}
func getFolderStatsHandler(w http.ResponseWriter, r *http.Request) {
	payload := struct {
		FolderName string `json:"folder_name"`
	}{}
	if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
		log.Println("error decoding JSON payload:", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	folderStats, err := storageService.GetFolderStats(payload.FolderName)
	if err != nil {
		log.Println("error getting folder stats:", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	writeJSON(w, folderStats)
}
