package api

import (
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/chespinoza/file-webservice/pkg/storage"
)

// Service API uservice structure
type Service struct {
	router      *chi.Mux
	addr        string
	storagePath string
}

// globallly available in the package
var storageService *storage.Storage

// Start init api service
func (s *Service) Start() error {

	storageService = storage.New(s.storagePath)

	s.router = chi.NewRouter()
	s.router.Post("/api/v1/files", newFileHandler)
	s.router.Get("/api/v1/files", getFileHandler)
	s.router.Put("/api/v1/files", replaceFileHandler)
	s.router.Delete("/api/v1/files", deleteFileHandler)
	s.router.Get("/api/v1/files/stats", getFolderStatsHandler)
	return http.ListenAndServe(s.addr, s.router)
}

// New - Service contructor
func New(addr, storagePath string) *Service {
	if len(addr) == 0 {
		log.Fatal("addr for http service is required", addr)
	}
	if len(storagePath) == 0 {
		log.Fatal("storage path is required:", storagePath)
	}
	service := &Service{
		addr:        addr,
		storagePath: storagePath,
	}
	return service
}
