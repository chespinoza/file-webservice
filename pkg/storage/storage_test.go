package storage

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/suite"
)

// Relative Path used for testing purposes
const testRepoPath = "../../filesystem"

type StorageTestSuite struct {
	suite.Suite
	storage *Storage
}

func TestStorageTestSuite(t *testing.T) {
	suite.Run(t, new(StorageTestSuite))
}

func (suite *StorageTestSuite) SetupTest() {
	wdPath, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	testPath := filepath.Join(wdPath, testRepoPath)
	suite.storage = New(testPath)

}

func (suite *StorageTestSuite) TearDownSuite() {
	pathToClean := filepath.Join(suite.storage.BasePath, "repo1")
	err := os.RemoveAll(pathToClean)
	if err != nil {
		panic(err)
	}
	pathToClean = filepath.Join(suite.storage.BasePath, "repo3")
	err = os.RemoveAll(pathToClean)
	if err != nil {
		panic(err)
	}
}

func (suite *StorageTestSuite) TestCreate() {
	err := suite.storage.Create("myfile.txt", "/repo1", []byte("Title: My Title\n\nHello\nBye"))
	suite.Nil(err)
}

func (suite *StorageTestSuite) TestFailWhenCreate() {
	err := suite.storage.Create("", "/repo1", []byte("Title: My Title\n\nHello\nBye"))
	suite.NotNil(err)
	suite.EqualError(err, "file name not provided")
}

func (suite *StorageTestSuite) TestGet() {
	fileContents, err := suite.storage.Get("myfile.txt", "/repo1")
	suite.Nil(err)
	suite.NotEmpty(fileContents)
}

func (suite *StorageTestSuite) TestFailWhenGet() {
	fileContents, err := suite.storage.Get("myfile.txt", "/repo")
	suite.NotNil(err)
	suite.Empty(fileContents)
}
func (suite *StorageTestSuite) TestReplace() {
	err := suite.storage.Replace("myfile.txt", "/repo1", []byte(`Title: My Title replace\n\nHello Replace\Bye`))
	suite.Nil(err)
}

func (suite *StorageTestSuite) TestDelete() {
	err := suite.storage.Create("to_delete.txt", "/repo1", []byte(""))
	suite.Nil(err)
	err = suite.storage.Delete("to_delete.txt", "/repo1")
	suite.Nil(err)
}

func (suite *StorageTestSuite) TestFailWhenDelete() {
	err := suite.storage.Delete("myfile.txt", "/repo3")
	suite.NotNil(err)
}

func (suite *StorageTestSuite) TestFolderStats() {
	err := suite.storage.Create("file1.txt", "/repo3", []byte("hi stop tre"))
	suite.Nil(err)
	err = suite.storage.Create("file2.txt", "/repo3", []byte("ade wef"))
	suite.Nil(err)
	stats, err := suite.storage.GetFolderStats("repo3")
	suite.T().Logf("%+v", stats)
	suite.Nil(err)
}
