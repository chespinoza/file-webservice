package storage

import (
	"errors"
	"io/ioutil"
	"math"
	"os"
	"path/filepath"
	"unicode"
)

const (
	// DefaultFileMode default permissions for created files
	DefaultFileMode = 0666
)

// FolderStats ..
type FolderStats struct {
	TotalFilesNum    int     `json:"number_of_files"`
	AlphaNumAverage  float64 `json:"alphanum_avg"`
	alphaNumVariance float64
	AlphaNumStdDev   float64 `json:"alphanum_std_dev"`
	alphaNums        int
	alphaNumsSqr     int
	WordLenAverage   float64 `json:"words_len_avg"`
	wordLenVariance  float64
	WordLenStdDev    float64 `json:"words_len_std_dev"`
	Words            int
	wordsSum         int
	TotalBytes       int64 `json:"total_bytes"`
}

// Storage structure to hold functionality around storage
type Storage struct {
	BasePath string
}

// New returns a new storage instance
func New(path string) *Storage {
	return &Storage{BasePath: path}
}

// Create create a file with a determined content into a determined path
// the file shouldn't exists before hand, it just create new files
func (s Storage) Create(fileName, path string, contents []byte) error {
	if err := s.checkInputParams(fileName, path); err != nil {
		return err
	}
	// directory path
	dirPath := filepath.Join(s.BasePath, path)
	// file directory path
	filePath := filepath.Join(dirPath, fileName)

	if _, err := os.Stat(filePath); !os.IsNotExist(err) {
		return errors.New("file already exists")
	}
	// create dir path if not exist
	if _, err := os.Stat(dirPath); os.IsNotExist(err) {
		err := os.Mkdir(dirPath, os.ModePerm)
		if err != nil {
			return errors.New("can't create folder: " + err.Error())
		}
	}

	err := ioutil.WriteFile(filePath, contents, DefaultFileMode)

	return err
}

// Get reads a file and returns its contents as []byte
func (s Storage) Get(fileName, path string) ([]byte, error) {
	realFilePath := filepath.Join(s.BasePath, path, fileName)
	content, err := ioutil.ReadFile(realFilePath)
	if err != nil {
		return nil, err
	}
	return content, nil
}

// Replace replace (truncating it) the content of a file, given a path and filename
func (s Storage) Replace(fileName, path string, content []byte) error {
	if err := s.checkInputParams(fileName, path); err != nil {
		return err
	}
	realFilePath := filepath.Join(s.BasePath, path, fileName)

	err := ioutil.WriteFile(realFilePath, content, os.ModePerm)
	if err != nil {
		return errors.New("error replacing content: " + err.Error())
	}
	return nil
}

// Delete deletes a file give a path and filename
func (s Storage) Delete(fileName, path string) error {
	realFilePath := filepath.Join(s.BasePath, path, fileName)
	err := os.Remove(realFilePath)
	return err
}

// nFiles returns the number of files in a dir
func (s Storage) nFiles(folderName string) (int, error) {
	realDirPath := filepath.Join(s.BasePath, folderName)
	files, err := ioutil.ReadDir(realDirPath)
	if err != nil {
		return 0, err
	}
	return len(files), nil
}

// GetFolderStats ..
func (s Storage) GetFolderStats(folderName string) (*FolderStats, error) {
	realDirPath := filepath.Join(s.BasePath, folderName)
	files, err := ioutil.ReadDir(realDirPath)
	if err != nil {
		return nil, err
	}
	nFiles := len(files)
	if nFiles == 0 {
		return nil, errors.New("no files in folder: " + folderName)
	}
	// reading files
	var alphaNumSum, alphaNumSqrSum, words, wordsLenSum, wordsSqrSum int
	var dirSize int64
	for _, file := range files {
		content, err := s.Get(file.Name(), folderName)
		if err != nil {
			return nil, err
		}
		var word []byte
		var alphaNums int
		// count chars in a file
		for i := 0; i < len(content); i++ {
			var isNotAlphaNum bool
			if unicode.IsNumber(rune(content[i])) || unicode.IsLetter(rune(content[i])) {
				alphaNums++
				if unicode.IsLetter(rune(content[i])) {
					word = append(word, content[i])
				}
			} else {
				isNotAlphaNum = true
			}

			var wordLen = len(word)
			if (isNotAlphaNum && wordLen > 0) || (i == len(content)-1 && wordLen > 0) {
				wordsLenSum += wordLen
				wordsSqrSum += wordLen * wordLen
				words++
				//fmt.Printf("into for:%s\n", word)
				word = []byte{}
			}

		}
		//fmt.Println("alphaNums:", alphaNums)
		alphaNumSum += alphaNums
		alphaNumSqrSum += alphaNums * alphaNums
		alphaNums = 0

		dirSize += file.Size()
		//fmt.Println(file.Name()) // debug
	}
	var folderStats FolderStats
	folderStats.alphaNums = alphaNumSum
	folderStats.alphaNumsSqr = alphaNumSqrSum
	folderStats.AlphaNumAverage = float64(alphaNumSum) / float64(nFiles)
	folderStats.alphaNumVariance = float64(nFiles*alphaNumSqrSum-alphaNumSum*alphaNumSum) / float64(nFiles)
	folderStats.AlphaNumStdDev = math.Sqrt(folderStats.alphaNumVariance)
	folderStats.TotalFilesNum = nFiles

	folderStats.WordLenAverage = float64(wordsLenSum) / float64(words)
	folderStats.wordLenVariance = float64((words*wordsSqrSum)-(wordsLenSum*wordsLenSum)) / float64(words)
	folderStats.WordLenStdDev = math.Sqrt(folderStats.wordLenVariance)
	folderStats.TotalBytes = dirSize
	folderStats.Words = words
	folderStats.wordsSum = wordsLenSum

	return &folderStats, nil
}

// checkInputParams utility method to check args
func (s Storage) checkInputParams(fileName, path string) error {
	if len(fileName) == 0 {
		return errors.New("file name not provided")
	}
	if len(path) == 0 {
		return errors.New("path not provided")
	}
	return nil
}

// checks if a file exists
func fileInfo(path string) (os.FileInfo, bool) {
	fileInfo, err := os.Stat(path)
	if err != nil {
		if os.IsNotExist(err) {
			return nil, false
		}
	}
	return fileInfo, true
}

func dirSize(filePath string) (size int64, err error) {
	err = filepath.Walk(filePath, func(_ string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			size += info.Size()
		}
		return err
	})
	return size, err
}
